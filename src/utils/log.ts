import { Vector3 } from "three";

export const debugBuffer = [];

export function log(input: string | any) {
  let text: string;
  if (typeof input === "string") {
    text = input;
  } else {
    text = JSON.stringify(
      input,
      (key, value) => {
        if (typeof value === "number") {
          return parseFloat(value.toFixed(2));
        } else if (value instanceof Vector3) {
          return value.toArray();
        }
        return value;
      },
      2
    );
  }
  debugBuffer.push(text);
}
