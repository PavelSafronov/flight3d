import { Vector3 } from "three";
import { ForceSource } from "./physics/forceSource";
import { GameContext } from "./gameInterfaces";
import { log } from "./utils/log";

export interface Props {
  pos: Vector3;
  rotation: Vector3;
  velocity: Vector3;
  heading: Vector3;
  up: Vector3;
  maxSpeed: number;
  maxSelfForce: number;

  radius: number;
  forceSource?: ForceSource;
}

export class Flyer {
  props: Props;

  public constructor(props: Props) {
    this.props = props;
  }

  public step(context: GameContext): void {
    const {
      props,
      props: { pos, velocity, heading, up, forceSource, maxSpeed },
    } = this;
    const { dt } = context;

    const { total } = forceSource.getForce(props, context);

    velocity.add(total.clone().multiplyScalar(dt));
    const truncatedVelocity = this.truncateVelocity(velocity, maxSpeed);
    velocity.set(truncatedVelocity.x, truncatedVelocity.y, truncatedVelocity.z);
    pos.add(velocity.clone().multiplyScalar(dt));

    if (velocity.lengthSq() > 0.0001) {
      props.heading = velocity.clone().normalize();
    }

    const zero = new Vector3();
    const right = new Vector3().crossVectors(heading, up);
    console.assert(right.distanceTo(zero) > 0.00001);
    console.assert(heading.distanceTo(zero) > 0.00001);
    console.assert(up.distanceTo(zero) > 0.00001);
    this.props.up = new Vector3().crossVectors(right, heading);
  }
  public draw(ctx: CanvasRenderingContext2D): void {}
  private truncateVelocity(velocity: Vector3, maxSpeed: number): Vector3 {
    const length = velocity.length();
    if (length < maxSpeed) {
      return velocity;
    } else {
      const unit = velocity.clone().normalize();
      return unit.multiplyScalar(maxSpeed);
    }
  }
}
