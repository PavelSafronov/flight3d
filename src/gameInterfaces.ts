import { Flyer } from "./flyer";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import Stats from "three/examples/jsm/libs/stats.module.js";
import { Vector3 } from "three";
import { Keyboard } from "./utils/keyboard";
export interface GameContext {
  dt: number;
}
export interface Props {
  groundSize: number;
  backgroundColor: number;
  flyer: Flyer;
  camera: {
    pos: THREE.Vector3;
    lookAt: THREE.Vector3;
  };
}
export interface InternalState {
  renderer: THREE.WebGLRenderer;
  camera: THREE.PerspectiveCamera;
  cameraControls: OrbitControls;
  radarCamera?: THREE.Camera;
  lastAnimateMs: number;
  stats: Stats;
  cube: THREE.Object3D;
  glider?: THREE.Object3D;
  scene: THREE.Scene;
  // gridGeometry: THREE.Geometry;
  keyboard: Keyboard;
}
export interface UIInputState {
  isAutopilotOn: boolean;
  isPaused: boolean;
  isFoggy: boolean;
  fogDistance: {
    near: number;
    far: number;
  };
  camera: {
    followGlider: boolean;
    followDistance: number;
  };
}
export interface GameState {
  isGameOver: boolean;
  target?: Vector3;
  userForce?: Vector3;
}
export interface State {
  internal: InternalState;
  uiInput: UIInputState;
  game: GameState;
}
