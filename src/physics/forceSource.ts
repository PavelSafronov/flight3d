import { GameContext } from "../gameInterfaces";
import { Props as FlyerProps } from "../flyer";
import { Vector3 } from "three";

export type Source = (ship: FlyerProps, gameContext: GameContext) => Vector3[];

export interface Props {
  stateToSourceMap: Map<string, Source>;
  currentState: string;
}

export class ForceSource {
  public props: Props;

  public getForce(
    ship: FlyerProps,
    context: GameContext
  ): { total: Vector3; components: Vector3[] } {
    const { currentState, stateToSourceMap } = this.props;

    if (stateToSourceMap.has(currentState)) {
      const source = stateToSourceMap.get(currentState)!;
      const forces = source(ship, context);
      const result = this.accumulateForces(forces, ship.maxSelfForce);
      return result;
    } else {
      return this.accumulateForces([], 0);
    }
  }
  private accumulateForces(
    forces: Iterable<Vector3>,
    maxSelfForce: number
  ): { total: Vector3; components: Vector3[] } {
    const components = [];
    const sum = new Vector3();
    for (const force of forces) {
      const magSoFar = sum.length();
      const magRemaining = maxSelfForce - magSoFar;
      if (magRemaining < 0) {
        break;
      }

      const magToAdd = force.length();
      let actual = force;
      if (magToAdd > magRemaining) {
        actual = force.clone().normalize().multiplyScalar(magRemaining);
      }
      components.push(actual);
      sum.add(actual);
    }
    return {
      total: sum,
      components,
    };
  }
  public setCurrentState(state: string) {
    this.props.currentState = state;
  }
  public addSource(state: string, source: Source): void {
    this.props.stateToSourceMap.set(state, source);
  }

  public constructor(
    props: Props = { currentState: "", stateToSourceMap: new Map() }
  ) {
    this.props = props;
  }
}
