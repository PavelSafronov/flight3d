# Info

This project is called Flight 3D and is an attempt at making a simple 3D gliding/flying sim.

# TODOs

This is a list of features that should be added to the project:

- Flight info should be displayed somewhere
- Flight controls
- Tilt camera controls
- Ground, clouds, mountains
- Air rippling effect under a cloud to show warm rising air
- Uplift around mountain ridges
- Auto-generated cities
- Radar in an arbitrary div
  - something like https://threejs.org/examples/#webgl_camera_logarithmicdepthbuffer
- Multiplayer, allow multiple people to fly and be seen by others
